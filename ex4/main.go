// main
package main

import (
	"ex4/math"
	"fmt"
)

func main() {
	sum := math.Add(1, 2, -3)
	fmt.Println(sum)
}
